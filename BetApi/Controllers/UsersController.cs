﻿using BetApi.Models;
using BetApi.Services;
using Microsoft.AspNetCore.Mvc;

namespace BetApi.Controllers
{
    [Route("api/v1/user")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUserService _usersService;
        public UsersController(IUserService usersService)
        {
            _usersService =usersService;
        }

       
        [HttpGet()]
        public IActionResult GetUsers(int? id)
        {
            return Ok(_usersService.GetUsers(id));
        }

        [HttpPost()]
        public IActionResult PostUsers(User user)
        {
            return Ok(_usersService.PostUsers(user));
        }

        [HttpPut()]
        public IActionResult PutUsers(User user)
        {
            return Ok(_usersService.PutUsers(user));
        }

        [HttpDelete()]
        public IActionResult DeleteUsers(int id)
        {
            return Ok(_usersService.DeleteUsers(id));
        }
    }
}