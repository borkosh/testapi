﻿using BetApi.Models;
using BetApi.Services;
using Microsoft.AspNetCore.Mvc;

namespace BetApi.Controllers
{
    [Route("api/v1/comment")]
    [ApiController]
    public class CommentsController : ControllerBase
    {
        private readonly ICommentService _commentsService;
        public CommentsController(ICommentService commentsService)
        {
            _commentsService = commentsService;
        }

        [HttpGet()]
        public IActionResult GetComments(int? id)
        {
            return Ok(_commentsService.GetComments(id));
        }

        [HttpPost()]
        public IActionResult PostComments(Comment comment)
        {
            return Ok(_commentsService.PostComments(comment));
        }

        [HttpPut()]
        public IActionResult PutComments(Comment comment)
        {
            return Ok(_commentsService.PutComments(comment));
        }

        [HttpDelete()]
        public IActionResult DeleteComments(int id)
        {
            return Ok(_commentsService.DeleteComments(id));
        }
    }
}