﻿using Microsoft.EntityFrameworkCore;

namespace BetApi.Models
{
    public class AppDBContext: DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Comment> Comments { get; set; }

        public AppDBContext (DbContextOptions options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
         
        }
               
    }
}
