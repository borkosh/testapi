﻿using System.Collections.Generic;


namespace BetApi.Models
{
    public class Response
    {
        public bool Success { get; set; } = true;

        public string Message { get; set; } = "Success";
    }

    public class UserResponse : Response
    {
        public List<User> Data { get; set; }
    }

    public class CommentResponse : Response
    {
        public List<Comment> Data { get; set; }
    }
}
