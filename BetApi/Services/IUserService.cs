﻿using BetApi.Models;

namespace BetApi.Services
{
    public interface IUserService
    {
        UserResponse GetUsers(int? id);
        Response PostUsers(User user);
        Response PutUsers(User user);
        Response DeleteUsers(int id);
    }
}
