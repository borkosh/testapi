﻿using BetApi.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace BetApi.Services
{
    public class UserService : IUserService
    {
        private readonly AppDBContext _context;

        public UserService(AppDBContext context)
        {
            _context = context;
        }

        public UserResponse GetUsers(int ?id)
        {
            var response = new UserResponse();
            try
            { 
                if (id != null)
                {
                    var param = id;
                    response.Data = _context.Users.FromSql("SELECT * from TEST.get_user({0}); ", param).ToList();
                }
                else
                {
                    response.Data = _context.Users.FromSql("SELECT * from test.get_user(NULL);").ToList();
                   
                }
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = ex.Message;
            }
            return response;
        }

        public Response PostUsers(User user)
        {
            Response response = new Response();
            try
            {
                var res = _context.Database.ExecuteSqlCommand("SELECT * from test.ins_user({0},{1});", user.Name, user.Email);                
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = ex.Message;
            }
            return response;
        }

        public Response PutUsers(User user)
        {
            Response response = new Response();
            try
            {
                var res  = _context.Database.ExecuteSqlCommand("SELECT * from test.upd_user({0},{1},{2});",user.Id, user.Name, user.Email);
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = ex.Message;
            }
            return response;
        }

        public Response DeleteUsers(int id)
        {
            Response response = new Response();
            try
            {
                var res = _context.Database.ExecuteSqlCommand("SELECT * from test.del_user({0});",id);
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = ex.Message;
            }
            return response;
        }
    }
}
