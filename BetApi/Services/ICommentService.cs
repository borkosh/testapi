﻿using BetApi.Models;

namespace BetApi.Services
{
    public interface ICommentService
    {
        CommentResponse GetComments(int? id);
        Response PostComments(Comment comment);
        Response PutComments(Comment comment);
        Response DeleteComments(int id);
    }
}
