﻿using BetApi.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BetApi.Services
{
    public class CommentService:ICommentService
    {
        private readonly AppDBContext _context;

        public CommentService(AppDBContext context)
        {
            _context = context;
        }
        public CommentResponse GetComments(int? id)
        {
            var response = new CommentResponse();
            try
            {
                if (id != null)
                {
                    var param = id;
                    response.Data = _context.Comments.FromSql("SELECT * from TEST.get_comment({0}); ", param).ToList();
                }
                else
                {
                    response.Data = _context.Comments.FromSql("SELECT * from TEST.get_comment(NULL);").ToList();

                }
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = ex.Message;
            }
            return response;
        }
        public Response PostComments(Comment comment)
        {
            Response response = new Response();
            try
            {
                var res = _context.Database.ExecuteSqlCommand("SELECT * from test.ins_comment({0},{1});", comment.Id_User, comment.Txt);
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = ex.Message;
            }
            return response;
        }

        public Response PutComments(Comment comment)
        {
            Response response = new Response();
            try
            {
                var res = _context.Database.ExecuteSqlCommand("SELECT * from test.upd_comment({0},{1},{2});", comment.Id, comment.Id_User, comment.Txt);
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = ex.Message;
            }
            return response;
        }

        public Response DeleteComments(int id)
        {
            Response response = new Response();
            try
            {
                var res = _context.Database.ExecuteSqlCommand("SELECT * from test.del_comment({0});", id);
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = ex.Message;
            }
            return response;
        }
    }
}
